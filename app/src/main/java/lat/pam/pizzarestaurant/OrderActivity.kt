package lat.pam.pizzarestaurant

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

class OrderActivity : AppCompatActivity() {
    private lateinit var tvOrderDetail:TextView
    private lateinit var tvInputOrder:TextView
    private lateinit var tvInputPengiriman:TextView
    private lateinit var tvPengiriman:TextView
    private lateinit var imgSquare:ImageView
    private lateinit var tvInputPengiriman2:TextView
    private lateinit var imgSquare2:ImageView
    private lateinit var imgTextBox:ImageView
    private lateinit var tvClose:TextView
    private lateinit var btnDone:Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order)

        tvOrderDetail = findViewById(R.id.tv_order_detail)
        tvInputOrder = findViewById(R.id.tv_input_order)
        tvInputPengiriman = findViewById(R.id.tv_input_pengiriman)
        tvPengiriman = findViewById(R.id.tv_pengiriman)
        imgSquare = findViewById(R.id.img_square)
        tvInputPengiriman2 = findViewById(R.id.tv_input_pengiriman2)
        imgSquare2 = findViewById(R.id.img_square2)
        imgTextBox = findViewById(R.id.img_textbox)
        tvClose = findViewById(R.id.tv_close)
        btnDone = findViewById(R.id.btn_done)
        btnDone.setOnClickListener {
            finish()
        }
    }
}