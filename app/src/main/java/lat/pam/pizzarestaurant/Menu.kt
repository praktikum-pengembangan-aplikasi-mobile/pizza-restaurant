package lat.pam.pizzarestaurant

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
@Parcelize
data class Menu(
    val name: String,
    val description: String,
    val photo: Int
) : Parcelable

