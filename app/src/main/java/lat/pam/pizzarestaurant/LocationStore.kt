package lat.pam.pizzarestaurant

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class LocationStore : AppCompatActivity() {

    private lateinit var tvPengguna:TextView
    private lateinit var tvStore2:TextView
    private lateinit var tvCihampelas:TextView
    private lateinit var tvLocation:TextView
    private lateinit var tvMaps:TextView
    private lateinit var btnMenu:Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_store)

        tvPengguna = findViewById(R.id.tv_pengguna)
        tvLocation = findViewById(R.id.tv_location)
        tvStore2 = findViewById(R.id.tv_store2)
        tvCihampelas = findViewById(R.id.tv_cihampelas)
        tvMaps = findViewById(R.id.tv_maps)
        btnMenu = findViewById(R.id.btn_menu)
        btnMenu.setOnClickListener {
            val menuIntent = Intent (this@LocationStore, MenuActivity::class.java)
            startActivity(menuIntent)
        }
    }
}