package lat.pam.pizzarestaurant

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private lateinit var tvStore:TextView
    private lateinit var tv1:TextView
    private lateinit var tv2:TextView
    private lateinit var tvYourName:TextView
    private lateinit var btnSubmit:Button
    private lateinit var edtName: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvStore = findViewById(R.id.tv_store)
        tv1 = findViewById(R.id.tv_1)
        tv2 = findViewById(R.id.tv_2)
        tvYourName = findViewById(R.id.tv_yourname)
        btnSubmit = findViewById(R.id.btn_submit)
        edtName = findViewById(R.id.edt_name)

        edtName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
            }
        })

        btnSubmit.setOnClickListener {
            if (edtName.text.isNullOrEmpty()) {
                Toast.makeText(this@MainActivity, "Nama tidak boleh dikosongkan", Toast.LENGTH_SHORT).show()
            } else {
                val locationIntent = Intent(this@MainActivity, LocationStore::class.java)
                startActivity(locationIntent)
            }
        }
    }
}